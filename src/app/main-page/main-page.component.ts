import { Component, OnInit } from '@angular/core';
import {BudgetItem} from '../shared/models/budget-item.model';
import {UpdateItem} from '../budget-item-list/budget-item-list.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  budgetItems: BudgetItem[] = new Array<BudgetItem>();
  totalBalance: number;

  constructor() { }

  ngOnInit(): void {
    this.totalBalance = 0;
  }

  sumValues() {
    let balance = 0;

    for (const item of this.budgetItems) {
      balance += item.amount;
    }
    this.totalBalance = balance;
  }

  addItem(newItem: BudgetItem) {
    this.budgetItems.push(newItem);
    this.sumValues();
  }

  removeItem(item: BudgetItem) {
    const indexToRemove = this.budgetItems.indexOf(item);
    this.budgetItems.splice(indexToRemove, 1);
    this.sumValues();
  }

  updateItem(updatedItem: UpdateItem) {
    this.budgetItems[this.budgetItems.indexOf(updatedItem.old)] = updatedItem.new;
    this.sumValues();
  }

}
