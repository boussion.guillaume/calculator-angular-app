# Calculator Angular App

Courte application développée dans l'idée de monter en compétences sur la technologie Angular.
Cette application n'a fait l'objet d'aucun projet professionnel ou scolaire, mais purement personnel.

**Screenshot de l'application :**

![ALT](/AppScreenshot.PNG)
